<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Job;
use App\Department;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::all();
        return view('employees.index', compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments = Department::all();
        $jobs = Job::all();
        $employees = Employee::all();
        return view('employees.create', compact('jobs','departments', 'employees'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $employee = new Employee();
        $employee -> employee_id = $request-> employee_id;
        $employee -> first_name = $request-> first_name;
        $employee -> last_name = $request-> last_name;
        $employee -> email = $request-> email;
        $employee -> phone_number = $request-> phone_number;
        $employee -> hire_date = $request-> hire_date;
        $employee -> job_id = $request-> job_id;
        $employee -> salary = $request->salary;
        $employee -> commission_pct = $request->commission_pct;
        $employee -> department_id = $request->department_id;
        $x = DB::table('departments')->where('department_id', $employee -> department_id);
        $employee -> manager_id = $x->value('manager_id');

        $employee ->save();

        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        //
    }
}
