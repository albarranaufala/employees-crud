<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $primaryKey = 'employee_id';
    public $timestamps = false;
    protected $fillable = [
        'employee_id','first_name', 'last_name', 'email', 'phone_number',
        'hire_date', 'job_id', 'salary', 'commission_pct', 'manager_id', 'department_id'
    ];
    public function job(){
        return $this->belongsTo('App\Job', 'job_id', 'job_id');
    }
    public function department(){
        return $this->belongsTo('App\Department', 'department_id', 'department_id');
    }
}