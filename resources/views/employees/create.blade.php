<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Add New Employee</title>
</head>

<body>
    <div class="container">
        <h1 class="mt-3">Add New Employee</h1>
        <form class="mt-5" method="post" action="/employees">
            @csrf
            <div class="form-row">
                <div class="form-group col-md-6">
                    <input name = 'employee_id' hidden type="text" class="form-control" id="employee_id" placeholder="Id" readonly="" value="{{$employees->max('employee_id')+1}}">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputFirstName">First Name</label>
                    <input name = 'first_name' type="text" class="form-control" id="first_name" placeholder="First Name">
                </div>
                <div class="form-group col-md-6">
                    <label for="inputLastName">Last Name</label>
                    <input name = 'last_name' required type="text" class="form-control" id="last_name" placeholder="Last Name">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputEmail">Email</label>
                    <input name = 'email' required type="email" class="form-control" id="email" placeholder="Email">
                </div>
                <div class="form-group col-md-6">
                    <label for="inputPhone">Phone</label>
                    <input  type="text" name = 'phone_number' class="form-control" id="phone_number" placeholder="Phone Number">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="Hire Date">Hire Date</label>
                    <input name = 'hire_date' required type="date" class="form-control" id="hire_date" placeholder="Hire Date">
                </div>
                <div class="form-group col-md-6">
                    <label for="inputJob">Job</label>
                    <select name = 'job_id' required id="job_id" class="form-control">
                        <option value="" disabled selected>Choose job</option>
                        @foreach($jobs as $job)
                        <option value="{{$job->job_id}}">{{$job->job_title}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputSalary">Salary</label>
                    <input name = 'salary' required type="number" class="form-control" id="salary" placeholder="Salary">
                </div>
                <div class="form-group col-md-6">
                    <label for="inputCommision">Commision</label>
                    <input step="0.01" name = 'commission_pct' type="number" class="form-control" id="commission_pct" placeholder="Commision">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputDepartment">Department</label>
                    <select name = 'department_id' id="department_id" class="form-control">
                        <option value="" selected>None</option>
                        @foreach($departments as $department)
                        <option value="{{$department->department_id}}">{{$department->department_name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Add Employee</button>
            <a href="/" class="card-link mx-3">Back</a>
        </form>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>